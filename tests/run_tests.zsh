#!/usr/bin/zsh
set -euo pipefail

# Initialize directories
echo '>> Initializing...'
for z in zeroed*.flac; do cp -- "$z" "con_from_orig/${z/#zeroed/z}"; done
# Aligned
for z in zeroed[2-4].flac; do cp -- zeroed1.flac "con_from_other/z1_from_${z/#zeroed/z}"; done
for z in zeroed[2-4].flac; do cp -- "$z" "con_from_other/z${z:6:1}_from_z1.flac"; done
# Unaligned
for z in zeroed[2-4].flac; do cp -- zeroedu1.flac "con_from_other/zu1_from_${z/#zeroed/z}"; done
for z in zeroed[2-4].flac; do cp -- "$z" "con_from_other/z${z:6:1}_from_zu1.flac"; done

# Run copy_over_nulls
echo '>> Running copy_over_nulls...'
[[ -x ../bin/copy_over_nulls ]] || exit 1
FAIL=0
for z in con_from_orig/*; do ../bin/copy_over_nulls -- orig.flac "$z" $((2*1024*1024)) || FAIL=1; done
for z in con_from_other/*; do ../bin/copy_over_nulls -- "zeroed${z##*_z}" "$z" $((2*1024*1024)) || FAIL=1; done
(( FAIL )) && { echo '>> ERROR: At least one copy_over_nulls failed.'; exit 1; }

# Compare results
echo '>> Checking the results...'
FAIL=0
for z in con_from_orig/z*; do cmp -- orig.flac "$z" || FAIL=1; done
# Aligned
for z in con_from_other/z1_from_z*; do F="${z##*_z}"; cmp -- "$z" "con_from_other/z${F%.*}_from_z1.flac" || FAIL=1; done
# Unaligned
for z in con_from_other/zu1_from_z*; do F="${z##*_z}"; cmp -- "$z" "con_from_other/z${F%.*}_from_zu1.flac" || FAIL=1; done
(( FAIL )) && { echo '>> ERROR: At least one comparison failed.'; exit 1; }

echo '>> Done! All tests passed.'
