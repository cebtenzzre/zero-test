/*
 * Copyright 2021 Cebtenzzre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

// In debug  mode, SMART_ASSERT is equivalent to assert(), with some extra compile-time checks on GNU compilers.
// When NDEBUG is defined, this hints to the compiler that the condition should never be false.
// NB: The expression passed to the macro is evaluated regardless of NDEBUG.  This behavior is different from standard assert().

// Define STRICTLY_NO_UB to avoid potentially calling a noreturn function that returns.
// Define NO_BUILTIN_CHOOSE_EXPR to avoid using __builtin_choose_expr on Clang.

#include <cassert>

// For __GLIBC__ macro
#if __has_include(<features.h>)
    #include <features.h>
#endif

#ifdef __GNUC__
    #define UNREACHABLE() __builtin_unreachable()
#elif !defined(STRICTLY_NO_UB)
    // Clang doesn't optimize __builtin_unreachable() properly, so call a noreturn function instead.
    // This should work on other compilers as well.
    #ifdef __clang__
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Winvalid-noreturn"
    #endif

    namespace detail_ { [[noreturn]] inline void unreachable() {} }
    #define UNREACHABLE() detail_::unreachable()

    #ifdef __clang__
        #pragma clang diagnostic pop
    #endif
#else
    #define UNREACHABLE() static_cast<void>(0)
#endif

#if defined(__clang__) && !defined(NO_BUILTIN_CHOOSE_EXPR)
    #define IF_KNOWN_CONSTANT(val, true_expr, false_expr) __builtin_choose_expr(__builtin_constant_p(val), (true_expr), (false_expr))

    #define ENSURE_NOT_ALWAYS_FALSE(cond) \
            static_assert(IF_KNOWN_CONSTANT(cond, cond, true), "Runtime assertion will always fail")
#elif defined(__GNUC__)
    #define C_STATIC_ASSERT(cond, type_name) \
        do { \
            typedef char type_name[static_cast<unsigned long>((!!(cond)) * 2 - 1)]; \
            static_cast<void>(static_cast<type_name *>(nullptr)); /* Avoid -Wunused-local-typedefs */ \
        } while (false)

    #define ENSURE_NOT_ALWAYS_FALSE(cond) \
        C_STATIC_ASSERT(__builtin_constant_p(cond) ? !!(cond) : true, error__runtime_assertion_will_always_fail)
#else
    #define ENSURE_NOT_ALWAYS_FALSE(cond) static_cast<void>(0)
#endif

#ifndef NDEBUG
    #define SMART_ASSERT(cond) \
        do { \
            /* Don't evaluate twice */ \
            if (false) { ENSURE_NOT_ALWAYS_FALSE(cond); } \
            assert(cond); \
        } while (false)

    #ifdef __GLIBC__
        // This is internal to GNU libc
        #define BUG() __assert_fail("Sanity check", __FILE__, __LINE__, __ASSERT_FUNCTION)
    #else
        #define BUG() assert(!"Sanity check")
    #endif
#else
    #define SMART_ASSERT(cond) \
        do { \
            /* Don't evaluate twice */ \
            if (false) { ENSURE_NOT_ALWAYS_FALSE(cond); } \
            /* Don't inhibit -Wparentheses */ \
            if (cond) {} else { UNREACHABLE(); } \
        } while (false)

    #define BUG() UNREACHABLE()
#endif
