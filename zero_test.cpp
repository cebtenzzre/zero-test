/*
 * Copyright 2021 Cebtenzzre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Check whether a file contains any non-zero bytes.
// Exit status: 0 if there is non-zero data, 1 if there is not, and 2 if an error was encountered.
// Returns 0 if there is nothing to do.
// Note to self: This is based on "FindUnsaved", but the shared code has generally improved here.

// This program does not depend on libstdc++.
// Requirements: C++11, _POSIX_VERSION >= 200112L

#if __cplusplus < 201103L
    #error C++11 support is necessary.
#endif

#ifdef __has_include
    #define has_unistd_h __has_include(<unistd.h>)
#else
    #define has_unistd_h (defined(__unix__) || (defined(__APPLE__) && defined(__MACH__)))
#endif

#if !has_unistd_h
    #error A POSIX-compatible host OS/environment is required.
#endif

#ifndef _POSIX_C_SOURCE
    #define _POSIX_C_SOURCE 200112L
#endif

#include <unistd.h>

#if !defined(_POSIX_VERSION) && defined(__has_include)
    #if __has_include(<features.h>)
        #include <features.h> // Maybe we'll find it here
    #endif
#endif

// Needed for posix_fadvise()
#if !defined(_POSIX_VERSION) || _POSIX_VERSION < 200112L
    #error This program requires _POSIX_VERSION >= 200112L.
#endif

// C includes
#include <cerrno>
#include <cstddef> // std::byte and other types
#include <cstdint>
#include <cstdio> // for perror, fprintf
#include <cstdlib> // for calloc, free, exit
#include <cstring> // for strcmp, strrchr, strerror

// POSIX includes
#include <err.h>
#include <fcntl.h>

// Local includes
#include "assert.hpp"




// -- Macros

#define KIB(n) (n * 1024ULL)

#define unlikely(cond) __builtin_expect(!!(cond), 0)

// Compiler extensions and language features

#ifdef __clang__
    #define DO_NOT_UNROLL _Pragma("clang loop unroll(disable)")
#elif defined(__GNUC__) && !defined(__INTEL_COMPILER)
    #define DO_NOT_UNROLL _Pragma("GCC unroll 0")
#else
    #define DO_NOT_UNROLL
#endif

#if defined(__cpp_if_constexpr) && __cpp_if_constexpr >= 201606L
    #define CONSTEXPR_IF(cond) if constexpr (cond)
#elif defined(__GNUC__)
    #define CONSTEXPR_IF(cond) if (__builtin_constant_p(cond) ? (cond) : (cond))
#else
    #define CONSTEXPR_IF(cond) if (cond)
#endif

#if defined(__cpp_lib_byte) && __cpp_lib_byte >= 201603L
    using Byte = std::byte;
#else
    using Byte = uint8_t;
#endif


// -- Constants

enum: int {
    EXIT_STATUS_SUCCESS = 0, // For --help or no input
    EXIT_STATUS_DATA_FOUND = 0,
    EXIT_STATUS_DATA_NOT_FOUND = 1,
    EXIT_STATUS_ERROR = 2
};

// 128 KiB read buffer
// See https://eklitzke.org/efficient-file-copying-on-linux
constexpr size_t READ_BUFFER_SIZE = KIB(128);

// Must be a power of two.
constexpr uintptr_t READ_BUFFER_ALIGN = 64;
#ifdef __GNUC__
    static_assert(__builtin_popcountll(READ_BUFFER_ALIGN) == 1);
#endif

// Basic types

template <size_t size_>
struct Buffer {
    Byte *data;
    static constexpr size_t size = size_;
};




namespace {
    constexpr size_t RUS_FAIL = SIZE_MAX;
    static_assert(READ_BUFFER_SIZE < RUS_FAIL, "READ_BUFFER_SIZE cannot be equal to SIZE_MAX");

    // Keeps reading on certain partial reads.  Returns RUS_FAIL on error.
    [[nodiscard, gnu::nonnull(2)]]
    size_t read_until_satisfied(int fd, Byte *buf, size_t count)
    {
        size_t bytes_read = 0;

        for (;;) {
            ssize_t ret;
            do {
                ret = read(fd, &buf[bytes_read], count - bytes_read);
            } while (unlikely(ret < 0) && errno == EINTR);

            if (unlikely(ret < 0))
                return RUS_FAIL; // Error

            bytes_read += static_cast<size_t>(ret);
            if (unlikely(ret == 0) || bytes_read >= count)
                return bytes_read; // EOF or full buffer

            // Read more
        }
    }

    template <size_t size>
    bool bufferIsZeroed_inner(Byte const *buf)
    {
        // Verify alignment
        SMART_ASSERT(reinterpret_cast<uintptr_t>(buf) % READ_BUFFER_ALIGN == 0);
        static_assert(size % READ_BUFFER_ALIGN == 0, "size must be a multiple of READ_BUFFER_ALIGN");

        uint64_t conjunction = 0;

    #ifdef __GNUC__
        auto *buf_al = static_cast<Byte const *>(__builtin_assume_aligned(buf, READ_BUFFER_ALIGN));

        uint64_t block;
        for (size_t i = 0; i < size; i += sizeof(block)) {
            __builtin_memcpy(&block, buf_al + i, sizeof(block));
            conjunction |= block;
        }
    #else
        for (size_t i = 0; i < size; ++i)
            conjunction |= buf[i];
    #endif

        return conjunction == 0ULL;
    }

    // Best chunk size found by benchmarking on Clang 8.0.0 and GCC 8.2.1.
    #ifdef __clang__
        constexpr size_t IZ_CHUNK_SIZE = KIB(1);
    #else
        constexpr size_t IZ_CHUNK_SIZE = 512;
    #endif

    // dynamic mode reads up to a given number of bytes, instead of all 128 KiB.
    // NB: dynamic mode reads in multiples of IZ_CHUNK_SIZE, so it may overread by up to IZ_CHUNK_SIZE - 1.
    //     Buffer must be pre-zeroed (i.e. calloc()ed) for this to work properly.
    template <bool dynamic>
    bool bufferIsZeroed(Byte const *buf, size_t bufsz = 0) {
        static_assert(READ_BUFFER_SIZE % IZ_CHUNK_SIZE == 0, "READ_BUFFER_SIZE must be a multiple of IZ_CHUNK_SIZE");
        CONSTEXPR_IF(dynamic) {
            static_cast<void>(bufsz); // Not used
        } else {
            SMART_ASSERT(bufsz != 0);
        }

        // Make sure limit is constexpr here if !dynamic
        #define LOOP(limit) \
            DO_NOT_UNROLL \
            for (size_t i = 0; i < limit; i += IZ_CHUNK_SIZE) \
                if (!bufferIsZeroed_inner<IZ_CHUNK_SIZE>(buf + i)) \
                    return false;

        CONSTEXPR_IF(dynamic) {
            LOOP(bufsz)
        } else {
            LOOP(READ_BUFFER_SIZE)
        }

        #undef LOOP

        return true;
    }

    char const *proper_basename(char const *filename) {
        char const *p = strrchr(filename, '/');
        return p ? p + 1 : filename;
    }

    [[noreturn]]
    void print_usage(char const *argv0, int exit_code) {
        char const *program_name = proper_basename(argv0);
        fprintf(stderr,
            "Usage: %s [--help|-h] [--] FILE\n"
            "Test FILE contains 1 or more non-zero bytes.\n"
            "\n"
            "Exit status is 0 if FILE contains non-zero bytes, 1 if not, 2 on error.\n",
            program_name);

        exit(exit_code);
    }
} // namespace

int main(int argc, char *argv[])
{
    if (unlikely(argc == 0))
        errx(EXIT_STATUS_ERROR, "argc is zero");

    // Parse options
    for (int i = 1; i < argc; ++i) {
        char const *arg = argv[i];
        if (strcmp(arg, "--") == 0)
            break; // End of options

        if (strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0)
            print_usage(argv[0], EXIT_STATUS_SUCCESS);

        if (unlikely(*arg == '-')) {
            warnx("unrecognized option '%s'", arg);
            print_usage(argv[0], EXIT_STATUS_ERROR);
        }
    }

    char const *fpath = nullptr;

    {
        // Parse operands
        bool parse_options = true;
        for (int i = 1; i < argc; ++i) {
            char const *arg = argv[i];
            if (parse_options && *arg == '-') {
                // Not a file argument, ignore it
                if (strcmp(arg, "--") == 0)
                    parse_options = false;
            } else {
                if (unlikely(fpath)) {
                    warnx("extra operand '%s'", arg);
                    print_usage(argv[0], EXIT_STATUS_ERROR);
                }
                fpath = arg;
            }
        }
    }

    if (unlikely(!fpath)) {
        warnx("missing operand");
        print_usage(argv[0], EXIT_STATUS_ERROR);
    }

    Buffer<READ_BUFFER_SIZE> read_buffer;
    constexpr uintptr_t extra_space = READ_BUFFER_ALIGN - 1;

    Byte *unaligned_buf_mem = static_cast<Byte *>(calloc(read_buffer.size + extra_space, sizeof(read_buffer.data[0])));
    if (unlikely(!unaligned_buf_mem))
        err(EXIT_STATUS_ERROR, "calloc");

    read_buffer.data = reinterpret_cast<Byte *>(
        (reinterpret_cast<uintptr_t>(unaligned_buf_mem) + extra_space) & ~(READ_BUFFER_ALIGN - 1));

    int slurp_file = open(fpath, O_RDONLY);
    if (unlikely(slurp_file < 0)) {
        warnx("cannot open '%s': %s", fpath, strerror(errno));
        free(unaligned_buf_mem);
        return EXIT_STATUS_ERROR;
    }

    int err = posix_fadvise(slurp_file, 0, 0, POSIX_FADV_SEQUENTIAL);
    if (unlikely(err < 0))
        perror("posix_fadvise"); // Not critical

    int ret;
    size_t bytes_read;
    bool first_check_done = false;
    for (;;) {
        bytes_read = read_until_satisfied(slurp_file, read_buffer.data, read_buffer.size);
        if (unlikely(bytes_read == RUS_FAIL)) {
            perror("read");
            close(slurp_file);
            free(unaligned_buf_mem);
            return EXIT_STATUS_ERROR;
        }

        if (bytes_read == 0) {
            // Either we never read any data, or all previous data was entirely zeros.
            ret = EXIT_STATUS_DATA_NOT_FOUND;
            break;
        }

        // Buffer partially filled and bufferIsZeroed is not hot / in icache; don't read all 128 KiB
        bool const do_partial = !first_check_done && bytes_read < read_buffer.size;

        if (!((do_partial ? bufferIsZeroed<true> : bufferIsZeroed<false>)(read_buffer.data, bytes_read))) {
            ret = EXIT_STATUS_DATA_FOUND;
            break;
        }
        if (unlikely(do_partial)) {
            ret = EXIT_STATUS_DATA_NOT_FOUND;
            break;
        }

        first_check_done = true;

        // No nonzero data found yet; loop again
    }

    close(slurp_file);
    free(unaligned_buf_mem);
    return ret;
}
