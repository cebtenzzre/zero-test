/*
 * Copyright 2021 Cebtenzzre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This program does not depend on libstdc++.
// Requirements: C++11, _POSIX_VERSION >= 200112L

#if __cplusplus < 201103L
    #error C++11 support is necessary.
#endif

#ifdef __has_include
    #define has_unistd_h __has_include(<unistd.h>)
#else
    #define has_unistd_h (defined(__unix__) || (defined(__APPLE__) && defined(__MACH__)))
#endif

#if !has_unistd_h
    #error A POSIX-compatible host OS/environment is required.
#endif

#ifndef _POSIX_C_SOURCE
    #define _POSIX_C_SOURCE 200112L
#endif

#if !defined(NO_GNU) && !defined(_GNU_SOURCE)
    #define _GNU_SOURCE // For SEEK_DATA and copy_file_range
#endif

#include <unistd.h>

#if !defined(_POSIX_VERSION) && defined(__has_include)
    #if __has_include(<features.h>)
        #include <features.h> // Maybe we'll find it here
    #endif
#endif

// Needed for posix_fadvise()
#if !defined(_POSIX_VERSION) || _POSIX_VERSION < 200112L
    #error This program requires _POSIX_VERSION >= 200112L.
#endif

// C includes
#include <cerrno>
#include <cinttypes>
#include <cstddef> // std::byte and other types
#include <cstdint>
#include <cstdio> // for perror, fprintf
#include <cstdlib> // for calloc, free, exit
#include <cstring> // for strcmp, strrchr, strerror

// POSIX includes
#include <err.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

// C++ includes
#include <algorithm>
#include <limits>
#include <tuple>

// Local includes
#include "assert.hpp"


// -- Macros

#define KIB(n) ((n) * 1024U)

#define likely(cond)   __builtin_expect(!!(cond), 1)
#define unlikely(cond) __builtin_expect(!!(cond), 0)

// Compiler extensions and language features

#ifdef __clang__
    #define DO_NOT_UNROLL _Pragma("clang loop unroll(disable)")
#elif defined(__GNUC__) && !defined(__INTEL_COMPILER)
    #define DO_NOT_UNROLL _Pragma("GCC unroll 0")
#else
    #define DO_NOT_UNROLL
#endif

#ifdef __GNUC__
    #define RESTRICT __restrict__
#else
    #define RESTRICT
#endif

#if defined(__cpp_lib_byte) && __cpp_lib_byte >= 201603L
    using Byte = std::byte;
#else
    using Byte = uint8_t;
#endif

// Asan

#ifdef __has_feature
    #if __has_feature(address_sanitizer)
        #define HAVE_ASAN 1
    #endif
#endif
#if defined(__SANITIZE_ADDRESS__) && !defined(HAVE_ASAN)
    #define HAVE_ASAN 1
#endif

#define HAVE_ASAN_INT_H 1
#ifdef __has_include
    #if !__has_include(<sanitizer/asan_interface.h>)
        #undef HAVE_ASAN_INT_H
    #endif
#endif

#if defined(HAVE_ASAN) && defined(HAVE_ASAN_INT_H)
    #include <sanitizer/asan_interface.h>
#endif


// -- Constants

enum: int {
    EXIT_STATUS_SUCCESS = 0,
    EXIT_STATUS_ERROR   = 1
};

// 128 KiB read buffer
// See https://eklitzke.org/efficient-file-copying-on-linux
constexpr size_t MAX_READ_BUFFER_SIZE = KIB(128);

// Must be a power of two.
constexpr uintptr_t READ_BUFFER_ALIGN = 64;
#ifdef __GNUC__
    static_assert(__builtin_popcountll(READ_BUFFER_ALIGN) == 1);
#endif

// Basic types

class Buffer {
    bool m_is_hole = false; // NB: Check this before touching this->data
    size_t m_size;

public:
    Byte *data = nullptr;

    Buffer(size_t size): m_size(size) {}

    [[nodiscard]] bool isHole() const { return m_is_hole; }
    [[nodiscard]] size_t size() const { return m_size; }

    void setHole(bool is_hole) {
        #if defined(HAVE_ASAN) && defined(HAVE_ASAN_INT_H)
            if (is_hole != m_is_hole) {
                if (is_hole) {
                    ASAN_POISON_MEMORY_REGION(data, size);
                } else {
                    ASAN_UNPOISON_MEMORY_REGION(data, size);
                }
            }
        #endif
        m_is_hole = is_hole;
    }

    [[nodiscard]] Byte       *end () const { return &data[size()]; }
    [[nodiscard]] Byte const *cend() const { return &data[size()]; }
};


namespace {
    template <typename A, typename B>
    auto min(A a, B b) { return a < b ? a : b; };

    constexpr size_t RUS_FAIL = SIZE_MAX;
    static_assert(MAX_READ_BUFFER_SIZE < RUS_FAIL, "MAX_READ_BUFFER_SIZE cannot be equal to SIZE_MAX");

    // Keeps reading on certain partial reads.  Returns RUS_FAIL on error.
    [[nodiscard, gnu::nonnull(2)]]
    size_t pread_until_satisfied(int fd, Byte *buf, size_t count, off_t offset)
    {
        size_t bytes_read = 0;

        for (;;) {
            ssize_t ret;
            do {
                ret = pread(fd, &buf[bytes_read], count - bytes_read,
                            offset + off_t(bytes_read));
            } while (unlikely(ret < 0) && errno == EINTR);

            if (unlikely(ret < 0))
                return RUS_FAIL; // Error

            bytes_read += size_t(ret);
            if (unlikely(ret == 0) || bytes_read >= count)
                return bytes_read; // EOF or full buffer

            // Read more
        }
    }

    [[maybe_unused, nodiscard, gnu::nonnull(2)]]
    size_t pwrite_until_satisfied(int fd, Byte const *buf, size_t const count, off_t const offset)
    {
        SMART_ASSERT(offset >= 0);
        SMART_ASSERT(size_t(offset) + count - 1
                         <= size_t(std::numeric_limits<off_t>::max()));

        size_t bytes_written = 0;

        for (;;) {
            ssize_t ret;
            do {
                ret = pwrite(fd, &buf[bytes_written], count - bytes_written,
                             offset + off_t(bytes_written));
            } while (unlikely(ret < 0) && errno == EINTR);

            if (unlikely(ret < 0))
                return RUS_FAIL; // Error

            bytes_written += size_t(ret);
            if (bytes_written >= count)
                return bytes_written; // No more data to write

            // Write more
        }
    }

    #ifndef NO_GNU
        [[nodiscard]]
        size_t pcopy_file_range_until_satisfied(
            int fd_in,  loff_t const off_in,
            int fd_out, loff_t const off_out,
            size_t const len, unsigned int const flags)
        {
            SMART_ASSERT(flags == 0);
            SMART_ASSERT(off_in >= 0);
            SMART_ASSERT(off_out >= 0);

            size_t bytes_copied = 0;
            loff_t off_in_cur  = off_in;
            loff_t off_out_cur = off_out;

            for (;;) {
                ssize_t ret;
                do {
                    // NB: off_in_cur and off_out_cur are updated by this call,
                    // but the file offsets of fd_in and fd_out are not.
                    ret = copy_file_range(
                        fd_in,  &off_in_cur,
                        fd_out, &off_out_cur,
                        len - bytes_copied, flags);
                } while (unlikely(ret < 0) && errno == EINTR);

                if (unlikely(ret < 0))
                    return RUS_FAIL; // Error

                bytes_copied += size_t(ret);
                if (unlikely(ret == 0))
                    warnx("zero-byte copy_file_range!");
                if (unlikely(ret == 0) || bytes_copied >= len)
                    return bytes_copied; // EOF or copy completed

                // Copy more
            }
        }
    #endif

    // Best chunk size found by benchmarking on Clang 8.0.0 and GCC 8.2.1.
    #ifdef __clang__
        constexpr unsigned int IZ_CHUNK_SIZE = KIB(1);
    #else
        constexpr unsigned int IZ_CHUNK_SIZE = 512;
    #endif

    static_assert(MAX_READ_BUFFER_SIZE % IZ_CHUNK_SIZE == 0, "MAX_READ_BUFFER_SIZE must be a multiple of IZ_CHUNK_SIZE");

    template <size_t size>
    bool bufferIsZeroed_inner(Byte const * const buf)
    {
        // Verify alignment
        static_assert(IZ_CHUNK_SIZE % READ_BUFFER_ALIGN == 0);
        SMART_ASSERT(uintptr_t(buf) % READ_BUFFER_ALIGN == 0);
        static_assert(size % READ_BUFFER_ALIGN == 0, "size must be a multiple of READ_BUFFER_ALIGN");

        uint64_t conjunction = 0;

    #ifdef __GNUC__
        auto *buf_al = static_cast<Byte const *>(__builtin_assume_aligned(buf, READ_BUFFER_ALIGN));

        uint64_t block;
        for (size_t i = 0; i < size; i += sizeof(block)) {
            __builtin_memcpy(&block, &buf_al[i], sizeof(block));
            conjunction |= block;
        }
    #else
        for (size_t i = 0; i < size; ++i)
            conjunction |= uint8_t(buf[i]);
    #endif

        return conjunction == 0ULL;
    }

    // dynamic mode reads up to a given number of bytes, instead of all 128 KiB.
    // NB: reads in multiples of IZ_CHUNK_SIZE, so it may overread by up to IZ_CHUNK_SIZE - 1.
    //     Buffer must be pre-zeroed (i.e. calloc()ed) for this to work properly.
    size_t bufferIsZeroedEx(Byte const *buf, size_t bufsz) {
        SMART_ASSERT(bufsz >= IZ_CHUNK_SIZE);
        SMART_ASSERT(bufsz % IZ_CHUNK_SIZE == 0);

        DO_NOT_UNROLL
        for (size_t i = 0; i < bufsz; i += IZ_CHUNK_SIZE)
            if (!bufferIsZeroed_inner<IZ_CHUNK_SIZE>(&buf[i]))
                return i;

        return SIZE_MAX;
    }

    bool bufferIsZeroed(Byte const *buf, size_t bufsz = 0)
        { return bufferIsZeroedEx(buf, bufsz) == SIZE_MAX; }

    char const *proper_basename(char const *filename) {
        char const *p = strrchr(filename, '/');
        return p ? p + 1 : filename;
    }

    [[noreturn]]
    void print_usage(char const *argv0, int exit_code) {
        char const *program_name = proper_basename(argv0);
        fprintf(stderr,
            "Usage: %s [--help|-h] [--] INFILE OUTFILE SECTION_SIZE\n"
            "Copy non-zero SECTION_SIZE chunks of INFILE into zeroed-out chunks of OUTFILE.\n"
            "SECTION_SIZE is in bytes.\n"
            "The first copy may be at any offset, but the rest are aligned to it.\n"
            "If alignment is unknown, SECTION_SIZE should be at most half of the actual minimum.\n",
            program_name);

        exit(exit_code);
    }

    [[gnu::nonnull(1)]]
    Byte const *memchr_inv(Byte const *str, unsigned char chr, size_t size)
    {
        if (size == 0) return nullptr;

        auto p = reinterpret_cast<unsigned char const *>(str);
        for (size_t i = 0; i < size; ++i) {
            if (p[i] != chr)
                return reinterpret_cast<Byte const *>(&p[i]);
        }

        return nullptr;
    }

    [[gnu::nonnull(1)]]
    Byte const *memrchr_inv(Byte const *str, unsigned char chr, size_t size)
    {
        if (size == 0) return nullptr;

        auto p = reinterpret_cast<unsigned char const *>(str);
        for (size_t i = size - 1;; --i) {
            if (p[i] != chr)
                return reinterpret_cast<Byte const *>(&p[i]);
            if (i == 0)
                return nullptr;
        }
    }

    // memptr will point to zeroed-out heap memory of at least size bytes
    // aligned to READ_BUFFER_ALIGN (which must be a power of 2).
    // Returned pointer must be freed by free().
    [[gnu::malloc]]
    void *aligned_calloc(Byte **memptr, size_t size) {
        constexpr auto extra_space = READ_BUFFER_ALIGN - 1;
        void *unaligned_mem = calloc(size + extra_space, sizeof(char));
        if (!unaligned_mem) return nullptr;

        auto as_uint = uintptr_t(unaligned_mem);
        *memptr = reinterpret_cast<Byte *>(
                (as_uint + extra_space) & ~(READ_BUFFER_ALIGN - 1));
        return unaligned_mem;
    }

    /*
     * Hole skipping: We mark the next hole's start and length, and if
     * we encounter it during a read, we skip over it and assume that much
     * of the file to be empty/zeroed data.
     */

    #if !defined(SEEK_DATA) || defined(NO_HOLE_FINDER)
        class HoleFinder
        {
            int const m_fd;
        #ifndef NDEBUG
            bool m_fnh_ran = false;
        #endif

        public:
            HoleFinder(int fd, [[maybe_unused]] off_t fileEnd): m_fd(fd) {}
            void findNextHoleAt([[maybe_unused]] off_t offset) {
            #ifndef NDEBUG
                assert(!m_fnh_ran); m_fnh_ran = true;
            #endif
            }
            [[nodiscard]] bool  holeFound() const { return false; }
            [[nodiscard]] off_t holeStart() const { BUG(); }
            [[nodiscard]] off_t holeEnd  () const { BUG(); }
        };
    #else
        class HoleFinder
        {
            int const m_fd;
            off_t const m_fileEnd;
            bool m_fnh_ran = false;
            bool m_holeFound;
            off_t m_holeStart, m_holeEnd;

        public:
            HoleFinder(int fd, off_t const fileEnd)
                    : m_fd(fd), m_fileEnd(fileEnd) {
                SMART_ASSERT(fileEnd >= 0);
            }

            void findNextHoleAt(off_t offset) {
                SMART_ASSERT(offset >= 0);
                SMART_ASSERT(!m_fnh_ran || m_holeFound);

                // Maybe we already found a hole here

                if (m_fnh_ran && m_holeFound && m_holeStart <= offset && m_holeEnd > offset)
                    return; // Our current state is still correct

                m_fnh_ran = true;

                // Look for the next hole

                if (offset < m_fileEnd) {
                    m_holeFound = false; // EOF, which we don't consider a hole
                    return;
                }

                off_t hole = lseek(m_fd, offset, SEEK_HOLE);
                if (hole < 0) err(EXIT_STATUS_ERROR, "lseek");
                m_holeFound = hole < m_fileEnd;

                if (m_holeFound) {
                    m_holeStart = hole;

                    off_t holeEnd_ = lseek(m_fd, hole, SEEK_DATA);
                    if (holeEnd_ < 0 && errno == ENXIO) {
                        holeEnd_ = m_fileEnd; // ENXIO if within a hole at the end of the file
                    } else if (holeEnd_ < 0) {
                        err(EXIT_STATUS_ERROR, "lseek");
                    }

                    assert(holeEnd_ <= m_fileEnd);
                    m_holeEnd = holeEnd_;
                }
            }

            [[nodiscard]] bool  holeFound() const { assert(m_fnh_ran  ); return m_holeFound; }
            [[nodiscard]] off_t holeStart() const { assert(holeFound()); return m_holeStart; }
            [[nodiscard]] off_t holeEnd  () const { assert(holeFound()); return m_holeEnd;   }
        };
    #endif

    struct File_Section { off_t start, len; off_t end() { return start + len; } };

    class Find_Unaligned_Empty_Section
    {
        static constexpr auto CHUNK = IZ_CHUNK_SIZE;
        int const fd;
        size_t const sectionSize;
        HoleFinder hf;
        off_t const fileEnd;
        off_t filePos, nextFilePos;
        void *unaligned_buf_mem;
        Buffer buffers[2];
        Buffer *rdbuf, *lastbuf;
        bool hit_eof = false;

    public:
        Find_Unaligned_Empty_Section(
            int fd_, off_t file_pos, off_t const file_end_abs, size_t const sectionSize_, size_t buffer_size
        ):
            fd(fd_), sectionSize(sectionSize_), hf(fd, file_end_abs), fileEnd(file_end_abs),
            buffers{ {buffer_size}, {buffer_size} },
            rdbuf(&buffers[0]), lastbuf(&buffers[1])
        {
            SMART_ASSERT(sectionSize_ % IZ_CHUNK_SIZE == 0);
            SMART_ASSERT(sectionSize_ <= size_t(std::numeric_limits<off_t>::max()));

            hf.findNextHoleAt(file_pos);

            Byte *aligned_mem = nullptr;
            unaligned_buf_mem = aligned_calloc(&aligned_mem, 2 * buffer_size);

            if (unlikely(!unaligned_buf_mem))
                err(EXIT_STATUS_ERROR, "calloc");

            buffers[0].data = aligned_mem;
            buffers[1].data = aligned_mem + buffers[0].size();
        }

        // We reference memory that is allocated by the constructor, copying and moving are not trivial
        Find_Unaligned_Empty_Section(Find_Unaligned_Empty_Section const &) = delete;
        Find_Unaligned_Empty_Section &operator=(Find_Unaligned_Empty_Section const &) = delete;
        Find_Unaligned_Empty_Section(Find_Unaligned_Empty_Section &&) = delete;
        Find_Unaligned_Empty_Section &operator=(Find_Unaligned_Empty_Section &&) = delete;

        ~Find_Unaligned_Empty_Section() noexcept {
            free(unaligned_buf_mem);
        }

    private:
        // Returns the number of bytes read.
        // Calls exit() on error.
        // Sets rdbuf->isHole() to indicate whether the current read is inside of a hole.
        // If rdbuf->isHole() is set to true, the contents of rdbuf->data must not be read.
        // Sets hit_eof to true if the buffer could not be filled completely.
        size_t getMore()
        {
            std::swap(rdbuf, lastbuf);

            off_t const readEnd = nextFilePos + off_t(rdbuf->size());

            while (hf.holeFound() && hf.holeEnd() <= nextFilePos)
                hf.findNextHoleAt(hf.holeEnd());

            if (hf.holeFound() && hf.holeStart() <= nextFilePos && hf.holeEnd() >= readEnd) {
                rdbuf->setHole(true);
                SMART_ASSERT(hf.holeStart() >= 0);
                // fprintf(stderr, "Stepping over hole at offset 0x%zX\n", size_t(hf.holeStart()));
                filePos = nextFilePos;
                nextFilePos += rdbuf->size();
                if (nextFilePos > fileEnd) {
                    hit_eof = true;
                    nextFilePos = fileEnd;
                }
                return rdbuf->size();
            }

            rdbuf->setHole(false);

            auto bytes_read = pread_until_satisfied(fd, rdbuf->data, rdbuf->size(), nextFilePos);
            if (unlikely(bytes_read == RUS_FAIL))
                err(EXIT_STATUS_ERROR, "pread");

            filePos = nextFilePos;
            nextFilePos += bytes_read;

            // If we hit EOF before, we should not continue to see data
            SMART_ASSERT(unlikely(bytes_read == 0) || !hit_eof);

            if (bytes_read < rdbuf->size())
                hit_eof = true;

            return bytes_read;
        }

        bool getMoreCond(
            size_t * RESTRICT const pos, size_t * RESTRICT const bufsiz,
            off_t * RESTRICT const pos_relfp)
        {
            if (*pos < *bufsiz) // Still data left
                return true;

            size_t bytes_read = getMore();
            SMART_ASSERT(*pos >= rdbuf->size() || hit_eof);
            if (bytes_read == 0) return false;

            SMART_ASSERT(filePos <= fileEnd);
            *pos = 0;
            *pos_relfp = filePos;
            *bufsiz = min(rdbuf->size(), size_t(fileEnd - filePos));

            return true;
        }

        ssize_t findAlignedStart(size_t const start_offset, size_t bufsiz)
        {
            if (rdbuf->isHole())
                return ssize_t(start_offset);

            for (size_t pos = start_offset; pos < bufsiz; pos += CHUNK) {
                if (bufferIsZeroed_inner<CHUNK>(&rdbuf->data[pos]))
                    return ssize_t(pos);
            }

            return -1;
        }

        off_t findExactStart(ssize_t astartpos)
        {
            off_t const alignedStart = filePos + astartpos;

            if (alignedStart == 0)
                return 0;

            if (alignedStart == filePos) {
                // Start of buffer, so look at the end of the last one
                SMART_ASSERT(!lastbuf->isHole()); // We know this chunk isn't all zeros
                auto sptr = memrchr_inv(lastbuf->cend() - CHUNK, '\0', CHUNK);
                SMART_ASSERT(sptr); // We know this chunk isn't all zeros
                if (sptr == lastbuf->cend() - 1) // Math gets weird on the border
                    return alignedStart;
                return filePos - off_t(rdbuf->size()) + (sptr - lastbuf->data);
            }

            SMART_ASSERT(!rdbuf->isHole()); // We know this chunk isn't all zeros
            auto sptr = memrchr_inv(&rdbuf->data[astartpos - CHUNK], '\0', CHUNK);
            SMART_ASSERT(sptr); // We know this chunk isn't all zeros
            return filePos + (sptr - rdbuf->data) + 1;
        }

    public:
        File_Section operator()(off_t newFilePos)
        {
            static constexpr File_Section ERROR_FS = {-1, -1};

            nextFilePos = newFilePos;
            bool at_start = nextFilePos == 0;
            hit_eof = false;
            auto const sectionSize_s = off_t(sectionSize);

            {
                size_t bytes_read = getMore();
                if (bytes_read == 0) return ERROR_FS;
            }

            size_t astartpos_offset = 0;
            for (;;) { // Finding start
                SMART_ASSERT(filePos <= fileEnd);
                size_t bufsiz = min(rdbuf->size(), size_t(fileEnd - filePos));

                ssize_t astartpos; // Aligned start relative to start of rdbuf
                off_t start; // Exact start relative to where the file pointer was when FUES was constructed
                if (at_start) {
                    // The range of zeros may begin at the end of another file
                    SMART_ASSERT(filePos == 0);
                    astartpos = 0;
                    start = 0;
                } else {
                    {
                        auto astpoff_relfp = filePos + off_t(astartpos_offset);
                        SMART_ASSERT(astpoff_relfp < fileEnd + CHUNK);
                        SMART_ASSERT(astartpos_offset == 0 || astpoff_relfp + CHUNK > fileEnd ||
                            (!rdbuf->isHole() && memrchr_inv(&rdbuf->data[astartpos_offset - CHUNK], '\0', CHUNK)));

                        if (!getMoreCond(&astartpos_offset, &bufsiz, &astpoff_relfp))
                            return ERROR_FS;
                    }

                    astartpos = findAlignedStart(astartpos_offset, bufsiz);
                    if (astartpos < 0) { // Not found in block
                        astartpos_offset = bufsiz;
                        at_start = false;
                        continue;
                    }

                    // Actual start; up to CHUNK - 1 before alignedStart
                    start = findExactStart(astartpos);

                    astartpos_offset = 0;
                }

                auto aendpos = size_t(astartpos); // (exclusive)
                // Start looking at the next chunk if we found a valid start
                if (!at_start) aendpos += CHUNK;

                for (;; aendpos += CHUNK) { // Finding end
                    off_t aend_relfp = filePos + off_t(aendpos);
                    SMART_ASSERT(aend_relfp < fileEnd + CHUNK);

                    if (!getMoreCond(&aendpos, &bufsiz, &aend_relfp))
                        return ERROR_FS;

                    // remaining_data can be small if we've hit EOF
                    size_t remaining_data = bufsiz - aendpos;
                    if (remaining_data < CHUNK || (!rdbuf->isHole() && !bufferIsZeroed_inner<CHUNK>(&rdbuf->data[aendpos])))
                    {
                        // Not a full 1K of zero bytes, could there still be enough?
                        size_t potential_extra = remaining_data >= CHUNK
                            ? CHUNK - 1 // Somwehere there is a non-zero byte
                            : remaining_data;
                        SMART_ASSERT(aendpos + potential_extra - 1 <= size_t(std::numeric_limits<off_t>::max()));
                        SMART_ASSERT(filePos + off_t(aendpos + potential_extra - 1) <= fileEnd);

                        if (at_start || hit_eof || (aend_relfp - start) + off_t(potential_extra) >= sectionSize_s)
                        {
                            // Maybe. Find the exact end.
                            auto eptr = rdbuf->isHole() ? nullptr : memchr_inv(&rdbuf->data[aendpos], '\0', potential_extra);

                            // !eptr: All zeros until the first guaranteed non-zero (or non-existent) byte
                            if (!eptr) eptr = &rdbuf->data[aendpos + potential_extra];

                            auto end = filePos + (eptr - rdbuf->data);
                            SMART_ASSERT(end <= fileEnd);
                            if (end > start && (at_start || hit_eof || end - start >= sectionSize_s))
                                return {start, end - start};
                        }

                        // If this is past cend, it will be dealt with at the top
                        astartpos_offset = aendpos + CHUNK;
                        at_start = false;

                        break; // Falls through to the top of the first loop
                    }

                    // Zero bytes, keep going...
                }

                // continuing...
            }
        }
    };

    class Copy_Over_Nulls
    {
        char const * const infile_path, * const outfile_path;
        size_t const out_section_size;
        void *unaligned_buf_mem;
        Buffer infile_rdbuf, outfile_rdbuf;
        int infile = -1, outfile = -1;

    public:
        Copy_Over_Nulls(char const *infile_path_, char const *outfile_path_, size_t out_section_size_):
            infile_path(infile_path_), outfile_path(outfile_path_),
            out_section_size(out_section_size_),
            infile_rdbuf(std::min(out_section_size_, MAX_READ_BUFFER_SIZE)),
            outfile_rdbuf(infile_rdbuf.size())
        {
            SMART_ASSERT(out_section_size_ % IZ_CHUNK_SIZE == 0);
            Byte *aligned_mem = nullptr;
            unaligned_buf_mem = aligned_calloc(&aligned_mem, infile_rdbuf.size() + outfile_rdbuf.size());

            if (unlikely(!unaligned_buf_mem))
                err(EXIT_STATUS_ERROR, "calloc");

            infile_rdbuf.data = aligned_mem;
            outfile_rdbuf.data = aligned_mem + infile_rdbuf.size();
        }

        ~Copy_Over_Nulls() noexcept {
            if (infile  != -1) close(infile);
            if (outfile != -1) close(outfile);
            free(unaligned_buf_mem);
        }

        static auto parse_args(int argc, char *argv[])
            -> std::tuple<char const *, char const *, size_t>
        {
            if (unlikely(argc == 0))
                errx(EXIT_STATUS_ERROR, "argc is zero");

            // Parse options
            for (int i = 1; i < argc; ++i) {
                char const *arg = argv[i];
                if (strcmp(arg, "--") == 0)
                    break; // End of options

                if (strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0)
                    print_usage(argv[0], EXIT_STATUS_SUCCESS);

                if (unlikely(*arg == '-')) {
                    warnx("unrecognized option '%s'", arg);
                    print_usage(argv[0], EXIT_STATUS_ERROR);
                }
            }

            char const *infile_path          = nullptr,
                       *outfile_path         = nullptr,
                       *out_section_size_str = nullptr;

            {
                // Parse operands
                bool parse_options = true;
                for (int i = 1; i < argc; ++i) {
                    char const *arg = argv[i];
                    if (parse_options && *arg == '-') {
                        // Not a file argument, ignore it
                        if (strcmp(arg, "--") == 0)
                            parse_options = false;
                    } else {
                        // Parse file argument
                        if (!infile_path) {
                            infile_path = arg;
                        } else if (!outfile_path) {
                            outfile_path = arg;
                        } else if (likely(!out_section_size_str)) {
                            out_section_size_str = arg;
                        } else {
                            warnx("extra operand '%s'", arg);
                            print_usage(argv[0], EXIT_STATUS_ERROR);
                        }
                    }
                }
            }

            for (auto str: {infile_path, outfile_path, out_section_size_str}) {
                if (!str) {
                    warnx("missing operand");
                    print_usage(argv[0], EXIT_STATUS_ERROR);
                }
            }

            long long out_section_size;
            {
                char *end;
                errno = 0;
                out_section_size = strtoll(out_section_size_str, &end, 10);
                if (end < argv[2] + strlen(argv[2])) {
                    warnx("bad section size");
                    print_usage(argv[0], EXIT_STATUS_ERROR);
                }
            }

            if (errno == ERANGE) {
                warnx("section size out of range");
                print_usage(argv[0], EXIT_STATUS_ERROR);
            }

            if (out_section_size <= 0 || size_t(out_section_size) % IZ_CHUNK_SIZE != 0) {
                warnx("outfile section size should be a positive multiple of %u bytes", IZ_CHUNK_SIZE);
                print_usage(argv[0], EXIT_STATUS_ERROR);
            }

            return {infile_path, outfile_path, out_section_size};
        }

    private:
        int open_file(char const *path, int flags) const
        {
            int file = open(path, flags);
            if (unlikely(file < 0)) {
                warn("cannot open '%s'", path);
                exit(EXIT_STATUS_ERROR);
            }

            int errval = posix_fadvise(file, 0, 0, POSIX_FADV_SEQUENTIAL);
            if (unlikely(errval < 0))
                perror("posix_fadvise"); // Not critical

            return file;
        }

    public:
        int exec()
        {
            infile  = open_file(infile_path,  O_RDONLY);
            outfile = open_file(outfile_path, O_RDWR);

            off_t outfile_size; // For seeking in infile
            bool files_same_device;
            {
                struct stat st_in;
                int res = fstat(infile, &st_in);
                if (res < 0)
                    err(1, "cannot stat '%s'", infile_path);

                if (S_ISDIR(st_in.st_mode))
                    errx(1, "%s: '%s'", strerror(EISDIR), infile_path);

                struct stat st_out;
                res = fstat(outfile, &st_out);
                if (res < 0)
                    err(1, "cannot stat '%s'", outfile_path);

                if (S_ISDIR(st_in.st_mode))
                    errx(1, "%s: '%s'", strerror(EISDIR), outfile_path);

                if (!S_ISREG(st_out.st_mode) && !S_ISBLK(st_out.st_mode))
                    errx(1, "not a seekable file: '%s'", outfile_path);

                if (st_in.st_dev == st_out.st_dev
                        && st_in.st_ino == st_out.st_ino) {
                    // Input files are identical, this program is a no-op
                    warnx("warning: input ('%s') and output ('%s') are the "
                          "same file, nothing to do\n",
                            infile_path, outfile_path);
                    return EXIT_STATUS_SUCCESS;
                }

                if (st_in.st_size != st_out.st_size) {
                    errx(1, "error: files '%s' and '%s' have different sizes",
                        infile_path, outfile_path);
                }

                if (st_in.st_size == 0) {
                    // Input files are empty, this program is a no-op
                    return EXIT_STATUS_SUCCESS;
                }

                outfile_size = st_out.st_size;
                files_same_device = st_in.st_dev == st_out.st_dev;
            }

            #define CHECK_FAIL(cond, fn)              \
                do {                                  \
                    if (unlikely(cond)) {             \
                        perror(fn);                   \
                        return EXIT_STATUS_ERROR;     \
                    }                                 \
                } while (false)

            off_t last_oes_start = 0;
            off_t file_pos = 0;

            HoleFinder hf{infile, outfile_size};
            hf.findNextHoleAt(file_pos);

            Find_Unaligned_Empty_Section fues{outfile, file_pos, outfile_size, out_section_size, infile_rdbuf.size()};
            for(;;) {
                File_Section outfile_empty_section = fues(file_pos);
                if (outfile_empty_section.start == -1) {
                    // outfile EOF
                    return EXIT_STATUS_SUCCESS;
                }
                SMART_ASSERT(outfile_empty_section.len > 0);
                SMART_ASSERT(outfile_empty_section.start == 0 || outfile_empty_section.start > last_oes_start);

                if (outfile_empty_section.start > 0) {
                    // Cache hint
                    int errval = posix_fadvise(infile, last_oes_start,
                            outfile_empty_section.start - last_oes_start,
                            POSIX_FADV_DONTNEED);
                    if (errval < 0)
                        perror("posix_fadvise");
                }
                last_oes_start = outfile_empty_section.start;

                // TODO: Trim the range so we aren't copying excessive zero
                // bytes into outfile

                file_pos = outfile_empty_section.start;
                File_Section remaining_outfile_empty_section = outfile_empty_section;
                do {
                    File_Section infile_copy_section = remaining_outfile_empty_section;

                    // Find a range with data

                    while (hf.holeFound() && hf.holeEnd() <= remaining_outfile_empty_section.start)
                        hf.findNextHoleAt(hf.holeEnd());

                    if (hf.holeFound() && hf.holeStart() < remaining_outfile_empty_section.end()) {
                        off_t roes_start = hf.holeEnd(), roes_end = remaining_outfile_empty_section.end();
                        remaining_outfile_empty_section = {roes_start, roes_end < roes_start ? 0 : roes_end - roes_start};

                        if (hf.holeStart() <= infile_copy_section.start) {
                            // There is a hole where we would start! Skip it.
                            SMART_ASSERT(hf.holeEnd() > infile_copy_section.start);
                            file_pos = remaining_outfile_empty_section.start;
                            continue;
                        }

                        // Copy the data before the hole (setting end to hf.holeStart())
                        infile_copy_section.len = hf.holeStart() - infile_copy_section.start;
                        SMART_ASSERT(infile_copy_section.end() == hf.holeStart());
                    } else {
                        // No hole -> full copy
                        remaining_outfile_empty_section = {remaining_outfile_empty_section.end(), 0};
                    }

                    // Write the infile data to outfile.

                    #ifndef NO_GNU
                        if (files_same_device) {
                            SMART_ASSERT(infile_copy_section.len >= 0);
                            size_t const outbytes_copied =
                                pcopy_file_range_until_satisfied(
                                    infile,  infile_copy_section.start,
                                    outfile, infile_copy_section.start,
                                    size_t(infile_copy_section.len), 0);
                            CHECK_FAIL(outbytes_copied == RUS_FAIL, "copy_file_range");

                            // Drop window from the page cache
                            int errval = posix_fadvise(outfile, infile_copy_section.start,
                                    infile_copy_section.len, POSIX_FADV_DONTNEED);
                            if (errval < 0)
                                perror("posix_fadvise");
                        } else
                    #endif
                    {
                        // Fallback copy
                        for (off_t pos = infile_copy_section.start;
                            pos < infile_copy_section.start + infile_copy_section.len;
                            pos += infile_rdbuf.size())
                        {
                            // Exclusive end, and length
                            off_t const end = min(pos + off_t(infile_rdbuf.size()),
                                infile_copy_section.start + infile_copy_section.len);
                            SMART_ASSERT(end >= pos);
                            auto const len = size_t(end - pos);

                            // Read from input
                            size_t const inbytes_read = pread_until_satisfied(infile, infile_rdbuf.data, len, pos);
                            CHECK_FAIL(inbytes_read == RUS_FAIL, "pread");
                            SMART_ASSERT(inbytes_read >= len); // EOF not expected

                            // Cache hint
                            int errval = posix_fadvise(infile, pos, off_t(len), POSIX_FADV_DONTNEED);
                            if (errval < 0)
                                perror("posix_fadvise");

                            if (bufferIsZeroed(infile_rdbuf.data, len))
                                continue; // Don't pwrite zero bytes into outfile

                            size_t const outbytes_written =
                                pwrite_until_satisfied(outfile, infile_rdbuf.data, len, pos);
                            CHECK_FAIL(outbytes_written == RUS_FAIL, "pwrite");
                            SMART_ASSERT(outbytes_written <= size_t(std::numeric_limits<off_t>::max()));

                            // Cache hint
                            errval = posix_fadvise(outfile, pos, off_t(len), POSIX_FADV_DONTNEED);
                            if (errval < 0)
                                perror("posix_fadvise");
                        }
                    }

                    // Seek to new data
                    file_pos = remaining_outfile_empty_section.start;
                } while (remaining_outfile_empty_section.start < remaining_outfile_empty_section.end());
            }

            #undef CHECK_FAIL
        }
    };
} // namespace

int main(int argc, char *argv[])
{
    auto args = Copy_Over_Nulls::parse_args(argc, argv);
    auto main_class = std::make_from_tuple<Copy_Over_Nulls>(args);
    return main_class.exec();
}
